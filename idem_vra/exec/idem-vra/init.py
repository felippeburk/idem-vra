def __init__(hub):
    hub.exec.idem-vra.ENDPOINT_URLS = ['']
    # The default is the first in the list
    hub.exec.idem-vra.DEFAULT_ENDPOINT_URL = ""

    # This enables acct profiles that begin with "idem-vra" for idem-vra modules
    hub.exec.idem-vra.ACCT = ["idem-vra"]

    def _get_version_sub(ctx, *args, **kwargs):
        api_version = ctx.acct.get("api_version", "latest")
        return hub.exec.idem-vra

    # Get the version sub dynamically from the ctx variable/acct
    hub.pop.sub.dynamic(hub.exec.idem-vra, _get_version_sub)
