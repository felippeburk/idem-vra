def __init__(hub):
    # This enables acct profiles that begin with "idem-vra" for states
    hub.states.idem-vra.ACCT = ["idem-vra"]
